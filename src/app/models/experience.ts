export interface Experience {
    type: string;
    date: string;
}
