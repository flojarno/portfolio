import { IconName } from "@fortawesome/fontawesome-svg-core";

export interface Technology {
    icon: IconName,
    name: string
}
