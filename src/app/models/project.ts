import { IconName } from "@fortawesome/fontawesome-svg-core";
import { Experience } from "./experience";
import { Technology } from "./technology";

export interface Project {
    id: string;  // using string for UUIDs or slugs, but can be a number if you prefer
    title: string;
    description: string;
    imageUrl: string;
    technologies: Technology[];
    galleryImages?: string[];
    experience: Experience;
    site?: 
    { 
        icon?: IconName,
        anchor: string,
        url: string
    } ;
}
