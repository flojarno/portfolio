import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './components/about/about.component';
import { PortfolioComponent } from './components/portfolio/portfolio.component';
import { ProjectsComponent } from './components/projects/projects.component';
import { ResumeComponent } from './components/resume/resume.component';
import { ContactComponent } from './components/contact/contact.component';
import { SkillsComponent } from './components/skills/skills.component';
import { ProjectPageComponent } from './components/project-page/project-page.component';

const routes: Routes = [
  {path: '', component: PortfolioComponent }, 
  {path: 'a-propos', component: AboutComponent},
  {path: 'competences', component: SkillsComponent},
  {path: 'projets', component: ProjectsComponent},
  {path: 'cv', component: ResumeComponent},
  {path: 'contact', component: ContactComponent},
  { path: 'projet/:id', component: ProjectPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
