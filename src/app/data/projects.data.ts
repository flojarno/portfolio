import { Project } from "../models/project"

export const PROJECTS: Project[] = [
  
  {
    id: 'e-fusion',
    title: 'Plate-forme e-commerce Énergie | e-Fusion',
    description: "Plate-forme de e-commerce pour mettre en lien producteurs d’énergie et consommateurs. Développement complet du projet incluant la conception (maquettage, modèles de données), frontend (Angular, Boostrap), backend (Spring Boot) et base de données (MySQL). Le site comprend une page principale de listing des producteurs que l'on peut filtrer et trier selon différents critères, une page vendeur dédiée, une page produit, un processus d'achat complet avec intégration du système de paiement Stripe et deux dashboards, producteur et utilisateur. Mise en application des méthodes agiles, notamment Scrum, avec des daily meetings, backlog, sprints et sprint reviews. Utilisation régulière des outils suivants : Git pour le versioning, gestionnaire de tâches Jira, groupe de discussion Discord.",
    imageUrl: '/assets/img/banner-efusion.jpg',
    technologies: [
      { icon: 'java', name: 'Java / Spring Boot' }, 
      { icon: 'angular', name: 'Angular' },
      { icon: 'bootstrap', name: 'Bootstrap' },
    ],
    galleryImages: [
      '/assets/img/home-cloud-energie.jpg',
      '/assets/img/page-vendeur-cloud-energie.jpg',
      '/assets/img/page-produit-cloud-energie.jpg'
    ], 
    experience: 
      { 
        type: 'Stage de 3 mois en entreprise', 
        date: 'Juin - Septembre 2023' 
      }, 
    site: 
    { 
        icon: 'github',
        anchor: "GitHub (accès sur demande)",
        url: 'https://github.com/flojarno/energy-marketplace'
    } 
  },
  {
    id: 'sacramento',
    title: 'Site vitrine Calligraphe | Anne Sacramento',
    description: "Site web réalisé avec WordPress pour une calligraphe professionnelle. Refonte d'un site existant avec un thème WordPress plus moderne et personnalisé avec CSS. Projet en groupe de 4 incluant maquettage et développement, en  mettant en application les méthodes agiles (backlog, sprints, daily meetings, gestionnaire de tâche Trello).",
    imageUrl: '/assets/img/banner-sacramento.jpg',
    technologies: [
      { icon: 'wordpress', name: 'WordPress' }, 
      { icon: 'css3-alt', name: 'CSS' },
    ],
    galleryImages: [
      '/assets/img/cours-sacramento.jpg',
      '/assets/img/galerie-sacramento.jpg',
      '/assets/img/grid-sacramento.jpg'
    ],
    experience: 
    { 
      type: 'Projet pédagogique de 15 jours en groupe', 
      date: 'Février 2023' 
    }
  },
  {
    id: 'immo-3-0',
    title: 'Plate-forme immobilière | Immo 3.0',
    description: "Plate-forme immobilière pour une agence. Projet de formation incluant du développement backend (Symfony) et frontend (React), avec utilisation du bundle EasyAdmin pour le back-office, d'API Platform pour les API et de MySQL pour la gestion de base de données. Le projet comprend une page d'accueil avec un listing des biens immobiliers que l'on peut filtrer en fonction du type de bien et de transaction, une page dédiée au bien, un formalaire d'inscription et de connexion, et un dashboard admin",
    imageUrl: '/assets/img/banner-immo.jpg',
    technologies: [
      { icon: 'symfony', name: 'Symfony' }, 
      { icon: 'react', name: 'React' },
      { icon: 'bootstrap', name: 'Bootstrap' },
    ],
    galleryImages: [],
    experience: 
    { 
      type: 'Projet pédagogique individuel de 3 semaines', 
      date: 'Avril 2023' 
    }, 
    site: 
    { 
      icon: 'github',
      anchor: "GitHub",
      url: 'https://github.com/flojarno/immo'
    } 
  },
  {
    id: 'indexing-tool',
    title: "Outil d'indexation d'URL | Google Indexer",
    description: "Google Indexer est un outil conçu pour aider les webmasters à s'assurer que les URL de leur site sont indexées par Google. L'outil scanne la sitemap d'un site web, parcourt les URL pour vérifier si elles sont indexées dans Google à l'aide de l'API Google Custom Search, et envoie les URL non indexées à l'API d'indexation de Google. Outil 100% en lignes de commande, sans interface pour le moment.",
    imageUrl: '/assets/img/indexing-api.jpg',
    technologies: [
      { icon: 'js-square', name: 'JavaScript' }, 
      { icon: 'node', name: 'Node.js' }
    ],
    galleryImages: [
      '/assets/img/google-indexer.jpg'
    ],
    experience: 
    { 
      type: 'Projet professionnel dans le cadre de mon activité webmarketing', 
      date: 'Mars 2023' 
    }, 
    site: 
    { 
      icon: 'github',
      anchor: "GitHub",
      url: 'https://github.com/flojarno/google-indexer'
    } 
  },
  {
    id: 'oppdig',
    title: 'Site de veille technologique | Opportunités Digitales',
    description: "Création et maintenance d'un site de veille sur les technologies du Web avec WordPress. Création d’interfaces utilisateur avec le constructeur de pages Elementor, JavaSrcipt et CSS. Ce site m'a permis de découvrir le CSS, notamment le design responsive avec les medias queries. Le site comprend des pages de blog (listes d'articles), des pages articles, guides, tests de produits, et utilise les bonnes pratiques d'optimisation pour les moteurs de recherches.",
    imageUrl: '/assets/img/banner-oppdig.jpg',
    technologies: [
      { icon: 'wordpress', name: 'WordPress' }, 
      { icon: 'elementor', name: 'Elementor' },
      { icon: 'js-square', name: 'JavaScript' },
      { icon: 'css3-alt', name: 'CSS' }
    ],
    galleryImages: [
      '/assets/img/article-oppdig.jpg',
      '/assets/img/blog-oppdig.jpg',
      '/assets/img/page-test-oppdig.jpg'
    ],
    experience: 
    { 
      type: 'Auto-entrepreneur, en continu', 
      date: 'Juin 2019 - Décembre 2022' 
    },
    site:  
    { 
      icon: 'github',
      anchor: "Site Web",
      url: 'https://www.opportunites-digitales.com/'
    } 
  },
  {
    id: 'chopin',
    title: 'Chronologie musicale de Chopin',
    description: "Chronologie verticale qui affiche les compositions de Chopin lorsque l'on scroll vers le bas. L'utilisateur peut jouer le morceau en cliquant sur le disque qui s'anime. Premier projet personnel, inachevé, qui m'a permis d'apprendre les bases du JavaScript, avec la gestion des évènements notamment. Ce projet ne reflète aucunement mes compétences en matière de design, c'est important de le préciser.",
    imageUrl: '/assets/img/chopin.jpg',
    technologies: [
      { icon: 'js-square', name: 'JavaScript' },
      { icon: 'css3-alt', name: 'CSS' }
    ],
    galleryImages: [],
    experience: 
    { 
      type: 'Projet personnel', 
      date: 'Août 2022' 
    }, 
    // site: 
    // { 
    //   icon: 'github',
    //   anchor: "Repo GitHub",
    //   url: 'https://github.com/flojarno/immo'
    // } 
  },
];