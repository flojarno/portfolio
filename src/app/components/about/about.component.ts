import { Component, OnDestroy, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit, OnDestroy {
  changingText = 'e alternance';
  private workTypes: string[] = ['e alternance', ' CDI'];
  private currentTextIndex = 0;
  
  private animationInterval: any; 
  private removeCharInterval: any;  // New property for removing characters interval
  private addCharInterval: any;     // New property for adding characters interval

  ngOnInit(): void {
    this.startAnimation();
  }

  ngOnDestroy(): void {
    clearInterval(this.animationInterval); 
    clearInterval(this.removeCharInterval); 
    clearInterval(this.addCharInterval);    
  }

  startAnimation(): void {
    this.animationInterval = setInterval(() => {
      this.animateTextChange();
    }, 5000);
  }

  animateTextChange() {
    const newText = this.workTypes[(this.currentTextIndex + 1) % this.workTypes.length];
    this.changeText(this.changingText, newText);
    this.currentTextIndex = (this.currentTextIndex + 1) % this.workTypes.length;
  }

  changeText(oldText: string, newText: string) {
    const interval = 100;
    let currentText = oldText;
    let i = 0;

    // Clear any existing intervals
    clearInterval(this.removeCharInterval);
    clearInterval(this.addCharInterval);

    this.removeCharInterval = setInterval(() => {
      currentText = currentText.slice(0, -1);
      this.changingText = currentText;

      if (currentText.length === 0) {
        clearInterval(this.removeCharInterval);

        this.addCharInterval = setInterval(() => {
          this.changingText += newText[i];
          i++;

          if (i === newText.length) {
            clearInterval(this.addCharInterval);
          }
        }, interval);
      }
    }, interval);
  }

  resetAnimation() {
    clearInterval(this.animationInterval);
    clearInterval(this.removeCharInterval);  
    clearInterval(this.addCharInterval);     
    this.currentTextIndex = 0;
    this.changingText = this.workTypes[this.currentTextIndex];
    this.startAnimation();
  }

  @HostListener('window:focus', ['$event'])
  onWindowFocus(event: FocusEvent): void {
    this.resetAnimation();
  }
}
