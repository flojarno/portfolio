import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { ResumeComponent } from './resume/resume.component';
import { ProjectsComponent } from './projects/projects.component';
import { SkillsComponent } from './skills/skills.component';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faGlobe } from '@fortawesome/free-solid-svg-icons';
import { faAngular, faBootstrap, faCss3Alt, faElementor, faGithub, faGoogle, faJava, faJsSquare, faNode, faNodeJs, faPhp, faReact, faSymfony, faWordpress } from '@fortawesome/free-brands-svg-icons';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ProjectPageComponent } from './project-page/project-page.component';

@NgModule({
  declarations: [
    AboutComponent,
    ContactComponent,
    ProjectsComponent,
    ResumeComponent,
    SkillsComponent,
    ProjectPageComponent

  ],
  imports: [
    CommonModule,
    RouterModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  exports: [
    AboutComponent,
    ContactComponent,
    ProjectsComponent,
    ResumeComponent,
    SkillsComponent,
  ]
})
export class ComponentsModule { 
  constructor(library: FaIconLibrary) {
    library.addIcons(
      faJava, 
      faPhp, 
      faAngular, 
      faSymfony, 
      faReact, 
      faBootstrap, 
      faWordpress, 
      faCss3Alt,
      faJsSquare,
      faElementor,
      faGlobe,
      faGithub,
      faNodeJs,
      faNode,
      faGoogle);
  }
}
