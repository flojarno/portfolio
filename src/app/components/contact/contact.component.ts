import { Component } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faEnvelope, faPhone } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent {

  contactForm: FormGroup;
  isSubmitted = false;

  constructor(private fb: FormBuilder, private http: HttpClient,library: FaIconLibrary) {
    library.addIcons(
      faEnvelope, faPhone),
    this.contactForm = this.fb.group({
      firstName: [''],
      lastName: [''],
      email: [''],
      message: ['']
    });
  }

  onSubmit() {
    const formData = this.contactForm.value;

    this.http.post('https://formspree.io/f/mdoryqno', formData).subscribe(response => {
      this.isSubmitted = true;
    });
  }
}
