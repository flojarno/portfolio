import { Component } from '@angular/core';
import { PROJECTS } from 'src/app/data/projects.data';
import { Project } from 'src/app/models/project';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent {
  projects = PROJECTS;
}
