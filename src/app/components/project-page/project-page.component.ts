import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PROJECTS } from 'src/app/data/projects.data';
import { Project } from 'src/app/models/project';
import { Renderer2, ElementRef } from '@angular/core';
import { IconName } from '@fortawesome/fontawesome-svg-core';


@Component({
  selector: 'app-project-page',
  templateUrl: './project-page.component.html',
  styleUrls: ['./project-page.component.css']
})
export class ProjectPageComponent {
  project?: Project;
  showPreview: string = ''; 
  isHovering: boolean = false;

  testShowPreview: string = '';
  testIsHovering: boolean = false;
  siteIcon?: IconName = undefined;


  constructor(
    private route: ActivatedRoute, 
    private renderer: Renderer2, 
    private el: ElementRef
) {}


  ngOnInit(): void {
    const projectId = this.route.snapshot.paramMap.get('id');
    console.log('Project ID:', projectId);
    this.project = PROJECTS.find(p => p.id === projectId);
    console.log('Found Project:', this.project);
    
    // Preload the images for the current project
    this.preloadImages();
  }


  preloadImages(): void {
    if (this.project && this.project.galleryImages) {
        this.project.galleryImages.forEach(image => {
            const img = new Image();
            img.src = image;
        });
    }
  }

  onHoverImage(img: string): void {
      this.showPreview = img;
      this.isHovering = true;
  }
  
  onHoverOut(): void {
      this.showPreview = "";
      this.isHovering = false;
  }  

  ngAfterViewInit(): void {
    const squares = this.el.nativeElement.querySelectorAll('.square');
    squares.forEach((square: any) => {
        this.renderer.listen(square, 'mouseover', (event) => {
            console.log("Mouseover event captured");
            let imgSrc;

            // Check if the event target is the image itself
            if(event.target.tagName === 'IMG') {
                imgSrc = event.target.src;
            } else {
                // Otherwise, attempt to find the child img of the square div
                const imgElement = event.target.querySelector('img');
                if(imgElement) {
                    imgSrc = imgElement.src;
                }
            }

            if(imgSrc) {
                this.onHoverImage(imgSrc);
            }
        });

        this.renderer.listen(square, 'mouseout', () => {
            this.onHoverOut();
        });
    });
  }

  onTestHover(): void {
    this.testShowPreview = '/assets/img/cours-sacramento.jpg';
    this.testIsHovering = true;
  }
  
  onTestHoverOut(): void {
    this.testShowPreview = '';
    this.testIsHovering = false;
  }
  
}
