import { Component } from '@angular/core';
import { FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faAngular, faBootstrap, faCss3, faCss3Alt, faElementor, faFigma, faInvision, faJava, faJsSquare, faPhp, faReact, faSymfony, faWordpress } from '@fortawesome/free-brands-svg-icons';
import { faDatabase} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.css']
})
export class SkillsComponent {
  constructor(library: FaIconLibrary) {
    library.addIcons(
      faAngular, 
      faReact, 
      faJava, 
      faPhp, 
      faSymfony, 
      faBootstrap, 
      faDatabase,  
      faFigma, 
      faWordpress, 
      faInvision,
      faJsSquare,
      faCss3,
      faCss3Alt,
      faElementor);
}
}
